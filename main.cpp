#include <benchmark/benchmark.h>
#include <complex>
#include <functional>
#include <memory>
#include <array>


constexpr int N_min = 1;
constexpr int N_max = 512;
constexpr int N_mul = 2;

//volatile double C_re = 0.01;
//volatile double C_im = 0.01;

//double C_re = 0.01;
//double C_im = 0.01;

//const double C_re = 0.01;
//const double C_im = 0.01;

constexpr double C_re = 0.01;
constexpr double C_im = 0.01;


static int compute(double re, double im, int max_iter)
{
	const auto c = std::complex<double> {re, im};

	auto z = std::complex<double> {0.0, 0.0};
	int iter = 0;

	for (; (iter < max_iter) && (std::abs(z) < 2.0); ++iter) {
		z = z * z + c;
		benchmark::DoNotOptimize(z);
	}

	return iter;
}

static void function_call(benchmark::State & state)
{
	const int max_iter = state.range(0);
	for (auto _ : state) {
		auto result = compute(C_re, C_im, max_iter);
		benchmark::DoNotOptimize(result);
	}
}

BENCHMARK(function_call)->Args({0})->RangeMultiplier(N_mul)->Range(N_min, N_max);

class base
{
public:
	virtual ~base() = default;
	virtual int compute(double re, double im, int max_iter) = 0;
};

class derived : public base
{
public:
	virtual int compute(double re, double im, int max_iter) override
	{
		return ::compute(re, im, max_iter);
	}
};

static void abstract_function_call_unique_ptr(benchmark::State & state)
{
	std::unique_ptr<base> p = std::make_unique<derived>();
	const int max_iter = state.range(0);
	for (auto _ : state) {
		auto result = p->compute(C_re, C_im, max_iter);
		benchmark::DoNotOptimize(result);
	}
}

BENCHMARK(abstract_function_call_unique_ptr)->Args({0})->RangeMultiplier(N_mul)->Range(N_min, N_max);


static void abstract_function_call_raw_ptr(benchmark::State & state)
{
	base * p = new derived;
	const int max_iter = state.range(0);
	for (auto _ : state) {
		auto result = p->compute(C_re, C_im, max_iter);
		benchmark::DoNotOptimize(result);
	}
	delete p;
}

BENCHMARK(abstract_function_call_raw_ptr)->Args({0})->RangeMultiplier(N_mul)->Range(N_min, N_max);


static void std_function_call(benchmark::State & state)
{
	std::function<int(double, double, int)> f = compute;
	const int max_iter = state.range(0);
	for (auto _ : state) {
		auto result = f(C_re, C_im, max_iter);
		benchmark::DoNotOptimize(result);
	}
}

BENCHMARK(std_function_call)->Args({0})->RangeMultiplier(N_mul)->Range(N_min, N_max);


static void function_ptr_call(benchmark::State & state)
{
	int (*f)(double, double, int) = compute;
	const int max_iter = state.range(0);
	for (auto _ : state) {
		auto result = f(C_re, C_im, max_iter);
		benchmark::DoNotOptimize(result);
	}
}

BENCHMARK(function_ptr_call)->Args({0})->RangeMultiplier(N_mul)->Range(N_min, N_max);


static void std_invoke_function(benchmark::State & state)
{
	const int max_iter = state.range(0);
	for (auto _ : state) {
		auto result = std::invoke(compute, C_re, C_im, max_iter);
		benchmark::DoNotOptimize(result);
	}
}

BENCHMARK(std_invoke_function)->Args({0})->RangeMultiplier(N_mul)->Range(N_min, N_max);


template <typename Func> static int call_templated_function(Func && f, int max_iter)
{
	return f(C_re, C_im, max_iter);
}

static void templated_function(benchmark::State & state)
{
	const int max_iter = state.range(0);
	for (auto _ : state) {
		auto result = call_templated_function(
			[](double re, double im, int max_iter) { return ::compute(re, im, max_iter); },
			max_iter);
		benchmark::DoNotOptimize(result);
	}
}

BENCHMARK(templated_function)->Args({0})->RangeMultiplier(N_mul)->Range(N_min, N_max);


static void lambda(benchmark::State & state)
{
	auto f = [](double re, double im, int max_iter) { return ::compute(re, im, max_iter); };
	const int max_iter = state.range(0);
	for (auto _ : state) {
		auto result = f(C_re, C_im, max_iter);
		benchmark::DoNotOptimize(result);
	}
}

BENCHMARK(lambda)->Args({0})->RangeMultiplier(N_mul)->Range(N_min, N_max);


static void iife(benchmark::State & state)
{
	const int max_iter = state.range(0);
	for (auto _ : state) {
		auto result = [=]() { return ::compute(C_re, C_im, max_iter); }();
		benchmark::DoNotOptimize(result);
	}
}

BENCHMARK(iife)->Args({0})->RangeMultiplier(N_mul)->Range(N_min, N_max);


BENCHMARK_MAIN();

