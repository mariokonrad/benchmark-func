.PHONY: prepare build run clean
prepare:
	cmake -B build -DCMAKE_CXX_COMPILER=g++-10 -DCMAKE_BUILD_TYPE=Release .

build:
	cmake --build build -j 4
	strip -s build/benchmark-func

run:
	build/benchmark-func

clean:
	rm -fr build

